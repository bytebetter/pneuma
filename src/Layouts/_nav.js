// import SvgIcon from 'react-icons-kit';

export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'ic_dashboard',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    {
      name: 'Planning',
      url: '/planning',
      icon: 'ic_insert_chart'
    },
  ]
};
