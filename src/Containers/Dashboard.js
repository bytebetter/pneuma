import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'
import RoundedButton from '../Components/RoundedButton';

import socketIOClient from "socket.io-client";

import ReactChartkick, { LineChart, AreaChart } from 'react-chartkick'
import Chart from 'chart.js'

ReactChartkick.addAdapter(Chart)

// var mqtt_logisenses_file = require('../Services/MQTTService');
// var mqtt = require('mqtt')




const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
  signining: false,
  endpoint: "http://localhost:3002",
  tempData: {
    "12:00:01": 35,
    "12:00:02": 35.2,
    "12:00:03": 35.4,
    "12:00:04": 34.9,
    "12:00:05": 35,
    "12:00:06": 35.2,
    "12:00:07": 35.5,
    "12:00:08": 35,
    "12:00:09": 35.3,
    "12:00:10": 35,
    "12:00:11": 35,
    "12:00:12": 35,
    "12:00:13": 35,
    "12:00:14": 35,
    "12:00:15": 35,
    "12:00:16": 35,
    "12:00:17": 35,
    "12:00:18": 35,
    "12:00:19": 35,
    "12:00:20": 35,
    "12:00:21": 35,
    "12:00:22": 35,
    "12:00:23": 36,
    "12:00:24": 37,
    "12:00:25": 38,
    "12:00:26": 37.8,
    "12:00:27": 38.2,
    "12:00:28": 38.1,
    "12:00:29": 38,
    "12:00:30": 38,
  },
  tempKeys: [],
  oxiData: {
    "12:00:01": 93,
    "12:00:02": 93.2,
    "12:00:03": 93.4,
    "12:00:04": 94.9,
    "12:00:05": 93,
    "12:00:06": 93.2,
    "12:00:07": 93.5,
    "12:00:08": 93,
    "12:00:09": 93.3,
    "12:00:10": 93,
    "12:00:11": 93,
    "12:00:12": 93,
    "12:00:13": 93,
    "12:00:14": 93,
    "12:00:15": 93,
    "12:00:16": 93,
    "12:00:17": 93,
    "12:00:18": 93,
    "12:00:19": 93,
    "12:00:20": 93,
    "12:00:21": 93,
    "12:00:22": 93,
    "12:00:23": 94,
    "12:00:24": 95,
    "12:00:25": 96,
    "12:00:26": 97.8,
    "12:00:27": 98.2,
    "12:00:28": 99.1,
    "12:00:29": 98,
    "12:00:30": 96,
  },
  oxiKeys: [],
  heartRateData: {
    "12:00:01": 93,
    "12:00:02": 93.2,
    "12:00:03": 93.4,
    "12:00:04": 94.9,
    "12:00:05": 93,
    "12:00:06": 93.2,
    "12:00:07": 93.5,
    "12:00:08": 93,
    "12:00:09": 93.3,
    "12:00:10": 93,
    "12:00:11": 98,
    "12:00:12": 99,
    "12:00:13": 110,
    "12:00:14": 115,
    "12:00:15": 120,
    "12:00:16": 120,
    "12:00:17": 115,
    "12:00:18": 110,
    "12:00:19": 105,
    "12:00:20": 95,
    "12:00:21": 93,
    "12:00:22": 93,
    "12:00:23": 94,
    "12:00:24": 95,
    "12:00:25": 96,
    "12:00:26": 97.8,
    "12:00:27": 98.2,
    "12:00:28": 99.1,
    "12:00:29": 98,
    "12:00:30": 96,
  },
  heartRateKeys: []
};

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  componentDidMount() {

    const socket = socketIOClient(this.state.endpoint)


    socket.on('connect', function () {
      console.log('socket connected')
    });

    socket.on('tempEvent', function (data) {

      let { tempData, tempKeys } = this.state

      let newTempData = { ...tempData }
      let newTempKeys = [...tempKeys]

      let time = new Date().toLocaleTimeString();

      if (Object.keys(newTempData).length >= 30) {
        let k = newTempKeys[0]
        delete newTempData[k]
        newTempKeys.shift()
      }

      if (data.object_Temp) {
        newTempData[time] = data.object_Temp
        newTempKeys.push(time)
      }

      this.setState({ tempData: newTempData, tempKeys: newTempKeys })
    }.bind(this));

    socket.on('o2satEvent', function (data) {

      let { oxiData, oxiKeys, heartRateData } = this.state

      let newOxiData = { ...oxiData }
      let newOxiKeys = [...oxiKeys]
      let newHeartRateData = { ...heartRateData }
      // let newHeartRateKeys = [...heartRateKeys]


      let time = new Date().toLocaleTimeString();


      if (Object.keys(newOxiData).length >= 30) {
        let k = newOxiKeys[0]
        delete newOxiData[k]
        delete newHeartRateData[k]
        newOxiKeys.shift()
      }

      if (data.o2Sat) {
        newOxiData[time] = data.o2Sat
        newHeartRateData[time] = data.heratRate
        newOxiKeys.push(time)
      }

      this.setState({ oxiData: newOxiData, oxiKeys: newOxiKeys, heartRateData: newHeartRateData })
    }.bind(this));


    socket.on('disconnect', function () {
      console.log('disconnected')
    });

  }

  componentWillMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  checkAuth() {

  }

  onSubmit = (event) => {

  }

  render() {

    const {
      email,
      password,
      error,
    } = this.state;

    const isInvalid =
      password === '' ||
      email === '';

    return (
      <div className="dashboard" style={{ backgroundColor: 'white' }}>

        <h2>Temp</h2>
        <LineChart data={this.state.tempData} height='200px' />

        <h2>Heart Rate</h2>
        <LineChart data={this.state.heartRateData} height='200px' colors={["#b00", "#666"]} />
        <h2>Oxigen Satuation</h2>
        <AreaChart data={this.state.oxiData} height='200px' />
      </div>
    );
  }
}

Dashboard.propTypes = {

};

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
