import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'
import RoundedButton from '../Components/RoundedButton';


const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

class Planning extends Component {

  constructor(props) {
    super(props);
    this.state = {};

  }


  componentWillMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  checkAuth() {

  }

  onSubmit = (event) => {

  }

  render() {
    return (
      <div className="Planning">
        Planning
      </div>
    );
  }
}

Planning.propTypes = {

};

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Planning)
